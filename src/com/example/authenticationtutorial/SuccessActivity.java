package com.example.authenticationtutorial;

import android.os.Bundle;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;

public class SuccessActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_success);
		
		Intent intent = getIntent();
		String username = intent.getStringExtra("name");
		
		final TextView success_text = (TextView)findViewById(R.id.successText);	

		success_text.setText("Welcome, " + username + "!");
	}

}
