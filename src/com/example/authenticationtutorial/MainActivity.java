package com.example.authenticationtutorial;

import java.io.InputStream;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	public static final String BASE_URL = "http://10.0.2.2:9000";
	public static final String BASE_MOBILE_URL = BASE_URL + "/mobile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final EditText username = (EditText)findViewById(R.id.usernameBox);
		final EditText password = (EditText)findViewById(R.id.passwordBox);

		Button button = (Button) findViewById(R.id.loginButton);
        button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new AuthenticateCredentialsTask(username.getText().toString(), password.getText().toString()).execute();
			}
        });
	}
	
	private static AuthenticateCredentialsTask mAuthenticateCredentialsTask = null;
	private class AuthenticateCredentialsTask extends AsyncTask<Void, Void, String> {
		private String username;
		private String password;

		public AuthenticateCredentialsTask(String username, String password) {
			this.username = username;
			this.password = password;

			if (mAuthenticateCredentialsTask != null) {
				mAuthenticateCredentialsTask.cancel(true);
				mAuthenticateCredentialsTask = null;
			}

			mAuthenticateCredentialsTask = this;
		}

		@Override
		protected void onPreExecute() {
			System.out.println("Authenticating user '" + username + "' with password '" + password + "'...");
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				String url = BASE_MOBILE_URL + "/authenticate?u=" + username + "&p=" + password;
				System.out.println(url);
				InputStream source = JSONHandler.retrieveStream(url);
				if (source == null) {
					return null;
				}

				return convertStreamToString(source);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(final String response) {
			if (response == null) {
				Toast.makeText(getApplicationContext(), "Connection to server failed!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (response.equals("failure")) {
				Toast.makeText(getApplicationContext(), "Invalid Username or Password!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			Intent intent = new Intent(MainActivity.this, SuccessActivity.class);
			intent.putExtra("name", username);
			startActivity(intent);
		}
		
		private String convertStreamToString(java.io.InputStream is) {
		    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		    return s.hasNext() ? s.next() : "";
		}
	}

}
